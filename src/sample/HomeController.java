package sample;


import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class HomeController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label imageButtomHome;

    @FXML
    void initialize() {
        assert imageButtomHome != null : "fx:id=\"imageButtomHome\" was not injected: check your FXML file 'app.fxml'.";

    }
}




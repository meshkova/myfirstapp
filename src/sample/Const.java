package sample;

public class Const {
    public static final String USER_TABLE = "users";
    public static final String USERS_ID= "idusers";
    public static final String USERS_FIRST_NAME = "firstName";
    public static final String USER_LAST_NAME= "lastName";
    public static final String USERS_USER_NAME = "userName";
    public static final String USERS_PASSWORD = "password";
    public static final String USERS_LOCATION = "location";
    public static final String USER_GENDER = "gender";

}
